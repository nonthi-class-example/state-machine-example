using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deposit : MonoBehaviour, IHaveResource
{
    [SerializeField] private int _maxHeld = 20;
    private int _gathered;

    public bool IsFull => _gathered >= _maxHeld;

    public event IHaveResource.ResourceAmountChangedDelegate OnResourceAmountChanged;

    public int GetCurrentResource()
    {
        return _gathered;
    }

    public int GetResourceCapacity()
    {
        return _maxHeld;
    }

    public int Add(int desiredAmount)
    {
        int amountToAdd = Mathf.Min(_maxHeld - _gathered, desiredAmount);
        _gathered += amountToAdd;

        OnResourceAmountChanged?.Invoke();

        return amountToAdd;
    }
}
