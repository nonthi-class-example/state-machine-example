
public interface IState
{
    void Tick(float dt);
    void OnEnter();
    void OnExit();
}
