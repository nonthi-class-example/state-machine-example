using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToResourceState : IState
{
    private Gatherer _gatherer;
    private Movement _movement;

    public MoveToResourceState(Gatherer gatherer, Movement movement)
    {
        _gatherer = gatherer;
        _movement = movement;
    }

    public void OnEnter()
    {
        _movement.SetTarget(_gatherer.TargetNode.transform);
        _gatherer.SetStateText("Moving.");
    }

    public void OnExit()
    {
        _movement.SetTarget(null);
        _gatherer.SetStateText(null);
    }

    public void Tick(float dt)
    {
    }
}
