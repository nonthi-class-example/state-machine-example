using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepositState : IState
{
    private Gatherer _gatherer;
    private float _resourcesPerSecond;

    private float _nextStoreResourceTime;

    public DepositState(Gatherer gatherer, float depositSpeed)
    {
        _gatherer = gatherer;
        _resourcesPerSecond = depositSpeed;
    }

    public void OnEnter()
    {
        _gatherer.SetStateText("Storing.");
    }

    public void OnExit()
    {
        _gatherer.SetStateText(null);
    }

    public void Tick(float dt)
    {
        if (_gatherer.TargetDeposit != null)
        {
            if (_nextStoreResourceTime <= Time.time)
            {
                _nextStoreResourceTime = Time.time + (1f / _resourcesPerSecond);
                _gatherer.StoreToDeposit(_gatherer.TargetDeposit, 1);
            }
        }
    }
}
