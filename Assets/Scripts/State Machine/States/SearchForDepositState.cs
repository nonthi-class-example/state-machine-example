using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SearchForDepositState : IState
{
    private float _searchInterval;
    private float _nextSearchTime;

    private Gatherer _gatherer;

    private int _failCount;
    public int failCount => _failCount;

    public SearchForDepositState(Gatherer gatherer, float searchInterval)
    {
        _gatherer = gatherer;
        _searchInterval = searchInterval;
    }

    public void OnEnter()
    {
        _gatherer.TargetDeposit = null;
        _failCount = 0;
        SetNextSearchTime();
        _gatherer.SetStateText("Searching for deposit.");
    }

    public void OnExit()
    {
        _failCount = 0;
        _gatherer.SetStateText(null);
    }

    public void Tick(float dt)
    {
        if (Time.time >= +_nextSearchTime)
        {
            _gatherer.TargetDeposit = GameObject.FindObjectsOfType<Deposit>()
                .OrderBy(t => Vector3.Distance(_gatherer.transform.position, t.transform.position))
                .Where(t => !t.IsFull)
                .FirstOrDefault();

            if (_gatherer.TargetDeposit == null)
            {
                _failCount++;
            }

            SetNextSearchTime();
        }
    }

    private void SetNextSearchTime()
    {
        _nextSearchTime = Time.time + _searchInterval;
    }
}
