using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatherResourceState : IState
{
    private Gatherer _gatherer;
    private float _resourcesPerSecond;

    private float _nextTakeResourceTime;

    public GatherResourceState(Gatherer gatherer, float gatherSpeed)
    {
        _gatherer = gatherer;
        _resourcesPerSecond = gatherSpeed;
    }

    public void OnEnter()
    {
        _gatherer.SetStateText("Gathering.");
    }

    public void OnExit()
    {
        _gatherer.SetStateText(null);
    }

    public void Tick(float dt)
    {
        if (_gatherer.TargetNode != null)
        {
            if (_nextTakeResourceTime <= Time.time)
            {
                _nextTakeResourceTime = Time.time + (1f / _resourcesPerSecond);
                _gatherer.TakeFromResourceNode(_gatherer.TargetNode, 1);
            }
        }
    }
}
