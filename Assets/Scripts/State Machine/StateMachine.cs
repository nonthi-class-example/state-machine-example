using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StateMachine
{
    private IState _currentState;

    private Dictionary<IState, List<Transition>> _transitions = new Dictionary<IState, List<Transition>>();
    private List<Transition> _currentTransitions = new List<Transition>();
    private List<Transition> _anyTransitions = new List<Transition>();

    private static List<Transition> EmptyTransitions = new List<Transition>(0);

    public void Tick(float dt)
    {
        var nextState = GetNextState();
        if (nextState != null)
        {
            SetState(nextState);
        }

        _currentState?.Tick(dt);
    }

    public void SetState(IState state)
    {
        if (state == _currentState)
        {
            return;
        }

        _currentState?.OnExit();
        _currentState = state;

        if (!_transitions.TryGetValue(_currentState, out _currentTransitions))
        {
            _currentTransitions = EmptyTransitions;
        }

        _currentState.OnEnter();
    }

    public void AddTransition(IState from, Func<IState> predicate)
    {
        if (!_transitions.TryGetValue(from, out var transitions))
        {
            transitions = new List<Transition>();
            _transitions[from] = transitions;
        }

        transitions.Add(new Transition(predicate));
    }

    public void AddAnyTransition(Func<IState> predicate)
    {
        _anyTransitions.Add(new Transition( predicate));
    }

    private class Transition
    {
        public Func<IState> Condition { get; }

        public Transition(Func<IState> condition)
        {
            Condition = condition;
        }
    }

    private IState GetNextState()
    {
        IState nextState = null;

        foreach (var transition in _anyTransitions)
        {
            nextState = transition.Condition();
            if (nextState != null)
            {
                return nextState;
            }
        }

        foreach (var transition in _currentTransitions)
        {
            nextState = transition.Condition();
            if (nextState != null)
            {
                return nextState;
            }
        }

        return null;
    }
}