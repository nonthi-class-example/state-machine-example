using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHaveResource
{
    int GetResourceCapacity();
    int GetCurrentResource();

    public delegate void ResourceAmountChangedDelegate();
    event ResourceAmountChangedDelegate OnResourceAmountChanged;
}
