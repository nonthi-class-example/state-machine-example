using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceNode : MonoBehaviour, IHaveResource
{
    [SerializeField] private int _totalAvailable = 20;

    private int _available;
    public bool IsDepleted => _available <= 0;

    public event IHaveResource.ResourceAmountChangedDelegate OnResourceAmountChanged;

    private void Awake()
    {
        _available = _totalAvailable;
    }

    public int Take(int desiredAmount)
    {
        if (_available <= 0)
        {
            return 0;
        }

        int amountToTake = Mathf.Min(desiredAmount, _available);
        _available -= amountToTake;

        OnResourceAmountChanged?.Invoke();

        return amountToTake;
    }

    public int GetCurrentResource()
    {
        return _available;
    }

    public int GetResourceCapacity()
    {
        return _totalAvailable;
    }
}
