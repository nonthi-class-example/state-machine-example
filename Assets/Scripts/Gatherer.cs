using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gatherer : MonoBehaviour, IHaveResource
{
    [SerializeField] private int _maxCarried = 20;
    [SerializeField] private float _interactionRange = 1f;
    [SerializeField] private float _gatherSpeed = 3f;
    [SerializeField] private float _depositSpeed = 8f;
    [SerializeField] private int _searchFailThreshold = 3;
    [SerializeField] private TextMeshPro _stateTextMesh;

    private int _gathered;

    public ResourceNode TargetNode { get; set; }
    public Deposit TargetDeposit { get; set; }

    public event IHaveResource.ResourceAmountChangedDelegate OnResourceAmountChanged;


    private Movement _movement;

    private StateMachine _stateMachine;

    SearchForResourceState _searchForResource;
    MoveToResourceState _moveToResource;
    GatherResourceState _gatherResource;
    SearchForDepositState _searchForDeposit;
    MoveToDepositState _moveToDeposit;
    DepositState _storeResource;

    private void Awake()
    {
        TryGetComponent(out _movement);
        SetStateText(string.Empty);

        _stateMachine = new StateMachine();

        _searchForResource = new SearchForResourceState(this, 1f);
        _moveToResource = new MoveToResourceState(this, _movement);
        _gatherResource = new GatherResourceState(this, _gatherSpeed);

        _searchForDeposit = new SearchForDepositState(this, 1f);
        _moveToDeposit = new MoveToDepositState(this, _movement);
        _storeResource = new DepositState(this, _depositSpeed);

        _stateMachine.AddTransition(_searchForResource, FoundAvaiableResource);
        _stateMachine.AddTransition(_moveToResource, MoveTargetNodeInvalid);
        _stateMachine.AddTransition(_moveToResource, ReachedTargetNode);
        _stateMachine.AddTransition(_gatherResource, GatherTargetNodeInvalid);
        _stateMachine.AddTransition(_gatherResource, HasFilledInventory);

        _stateMachine.AddTransition(_searchForDeposit, FoundAvaiableDeposit);
        _stateMachine.AddTransition(_moveToDeposit, MoveTargetDepositInvalid);
        _stateMachine.AddTransition(_moveToDeposit, ReachedTargetDeposit);
        _stateMachine.AddTransition(_storeResource, TargetDepositInvalid);
        _stateMachine.AddTransition(_storeResource, HasEmptiedInventory);

        _stateMachine.AddTransition(_searchForResource, CannotFindNode);
        _stateMachine.AddTransition(_searchForDeposit, CannotFindDeposit);

        _stateMachine.SetState(_searchForResource);
    }

    private void FixedUpdate()
    {
        _stateMachine.Tick(Time.deltaTime);
    }

    public int GetCurrentResource()
    {
        return _gathered;
    }

    public int GetResourceCapacity()
    {
        return _maxCarried;
    }

    public void TakeFromResourceNode(ResourceNode node, int desiredAmount)
    {
        int amountToTake = Mathf.Min(desiredAmount, _maxCarried - _gathered);
        _gathered += node.Take(amountToTake);
        OnResourceAmountChanged?.Invoke();
    }

    public void StoreToDeposit(Deposit deposit, int desiredAmount)
    {
        int amountToStore = Mathf.Min(desiredAmount, _gathered);
        _gathered -= deposit.Add(amountToStore);
        OnResourceAmountChanged?.Invoke();
    }

    public bool CanGatherFromNode(ResourceNode node)
    {
        if (node == null) return false;
        if (node.IsDepleted) return false;
        if (Vector3.Distance(node.transform.position, transform.position) >= _interactionRange) return false;

        return true;
    }

    public bool CanStoreToDeposit(Deposit deposit)
    {
        if (deposit == null) return false;
        if (deposit.IsFull) return false;
        if (Vector3.Distance(deposit.transform.position, transform.position) >= _interactionRange) return false;

        return true;
    }

    public void SetStateText(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            _stateTextMesh.text = "";
        }
        else
        {
            _stateTextMesh.text = text;
        }
    }

    private IState FoundAvaiableResource()
    {
        if (TargetNode != null)
        {
            return _moveToResource;
        }

        return null;
    }

    private IState MoveTargetNodeInvalid()
    {
        if (TargetNode == null || TargetNode.IsDepleted)
        {
            return _searchForResource;
        }

        return null;
    }

    private IState ReachedTargetNode()
    {
        if (CanGatherFromNode(TargetNode))
        {
            return _gatherResource;
        }

        return null;
    }

    private IState GatherTargetNodeInvalid()
    {
        if (!CanGatherFromNode(TargetNode) && _gathered < _maxCarried)
        {
            return _searchForResource;
        }

        return null;
    }

    private IState HasFilledInventory()
    {
        if (_gathered >= _maxCarried)
        {
            return _searchForDeposit;
        }

        return null;
    }

    private IState FoundAvaiableDeposit()
    {
        if (TargetDeposit != null)
        {
            return _moveToDeposit;
        }

        return null;
    }

    private IState MoveTargetDepositInvalid()
    {
        if (TargetDeposit == null || TargetDeposit.IsFull)
        {
            return _searchForDeposit;
        }

        return null;
    }

    private IState ReachedTargetDeposit()
    {
        if (CanStoreToDeposit(TargetDeposit))
        {
            return _storeResource;
        }

        return null;
    }

    private IState TargetDepositInvalid()
    {
        if (!CanStoreToDeposit(TargetDeposit) && _gathered > 0)
        {
            return _searchForDeposit;
        }

        return null;
    }

    private IState HasEmptiedInventory()
    {
        if (_gathered <= 0)
        {
            return _searchForResource;
        }

        return null;
    }

    private IState CannotFindNode()
    {
        if (_searchForResource.failCount >= _searchFailThreshold && _gathered > 0)
        {
            return _searchForDeposit;
        }

        return null;
    }

    private IState CannotFindDeposit()
    {
        if (_searchForDeposit.failCount >= _searchFailThreshold && _gathered < _maxCarried)
        {
            return _searchForResource;
        }

        return null;
    }
}
