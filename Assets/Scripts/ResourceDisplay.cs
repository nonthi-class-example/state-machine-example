using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshPro _textMesh;

    private IHaveResource _resourceOwner;

    private void Awake()
    {
        _resourceOwner = GetComponent<IHaveResource>();
        if (_resourceOwner != null)
        {
            _resourceOwner.OnResourceAmountChanged += UpdateDisplay;
        }
    }

    private void Start()
    {
        UpdateDisplay();
    }

    private void OnDestroy()
    {
        if (_resourceOwner != null)
        {
            _resourceOwner.OnResourceAmountChanged -= UpdateDisplay;
        }
    }

    private void UpdateDisplay()
    {
        _textMesh.text = $"{_resourceOwner.GetCurrentResource()}/{_resourceOwner.GetResourceCapacity()}";
    }
}
